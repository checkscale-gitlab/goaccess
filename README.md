# GoAccess

Small docker image to run [GoAccess](https://goaccess.io/) web report
with GeoIP and `gz` log files support.

## Usage

```bash
docker run --rm -it -v $PWD/logs:/opt/log -p 7889:7889 goaccess
```

Open your browser at http://mydomain.com:7889

![GoAccess interface](img/goaccess.png)

It will automatically read all `access.log*` files :

* access.log
* access.log.1
* access.log.2.gz
* access.log.3.gz
* ...

To parse differently named files :

```bash
docker run --rm -it -v $PWD/logs:/opt/log -p 7889:7889 goaccess nextcloud-access
```

* nextcloud-access.log
* nextcloud-access.log.1
* nextcloud-access.log.2.gz
* nextcloud-access.log.3.gz
* ...

To stop server, just hit `CTRL`+`C`.

## Description

This image is based on the [gregyankovoy/goaccess](https://hub.docker.com/r/gregyankovoy/goaccess).

It only adds the `gz` files support.
